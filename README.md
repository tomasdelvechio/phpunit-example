# Notas sobre PHPUnit

Referencia: https://jtreminio.com/blog/unit-testing-tutorial-part-i-introduction-to-phpunit/

Se utilizara PHPUnit 9 y se actualiza la serie original allí donde haga falta.

## Setup inicial

Consola + Composer: Forma de instalación de phpunit

Xdebug: Deseable pero no imprescindible

Instalar PHPUnit globalmente:

```bash
composer global require phpunit/phpunit
```

Esto supone que los binarios de composer se encuentran disponibles en la variable `$PATH`:

```bash
export PATH=~/.composer/vendor/bin:$PATH
# or
export PATH=~/.config/composer/vendor/bin:$PATH
# check your path installation
```

referencias sobre instalar globalmente con composer: https://akrabat.com/global-installation-of-php-tools-with-composer/

Comprobar que phpunit quedo accesible

```bash
$ phpunit --version
PHPUnit 9.1.1 by Sebastian Bergmann and contributors.
```

## Estructura básica del proyecto

```
src/
tests/
composer.json
phpunit.xml
```

Contenido inicial del archivo `composer.json`:

```json
{
    "require": {
    },
    "require-dev": {
    },
    "autoload": {
        "psr-4": {
            "PhpUnitTutorial\\": ["src", "tests"]
        }
    }
}
```

Contenido inicial del archivo `phpunit.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<phpunit colors="true">
    <testsuites>
        <testsuite name="Application Test Suite">
            <directory>./tests/</directory>
        </testsuite>
    </testsuites>
</phpunit>
```

## Configuración inicial de PHPUnit

PHP Unit acepta muchos argumentos a su comando. Pueden incluirse en la linea de comandos, pero existe una forma de no tener que escribirlos en cada comando, vía el archivo `phpunit.xml`.

## Convenciones de PHPUnit

Es posible no seguir estas convenciones, pero son ampliamente aceptadas y utilizadas por la comunidad de testing.

### Árbol de directorios

Si tenemos estructurado nuestro código de la siguiente manera:

```
./src/Foo.php
./src/Bar.php
./src/Controller/Baz.php
```

Los tests deberían tener la forma:

```
./tests/FooTest.php
./tests/BarTest.php
./tests/Controller/BazTest.php
```

Es decir, el árbol de directorios bajo `src` y `tests` deberían ser idénticos. Los nombres de archivos deberían serlo también, salvo que en el caso de los creados bajo `tests` deben incluir el sufijo `Test`.

### Nombre de clases

Una convención no siempre respetada es que el nombre de la clase debería coincidir con el nombre del archivo (sin el `.php`, obvio). Esto aplica a proyectos donde no se usen tests.

### Nombres de metodos

Varias convenciones:

- prefijar todos los métodos de test con el string `test`.
- Debería incluirse el nombre del método a testear pero se puede agregar mas detalles del tipo de test.

Ejemplo:

Tengo un método llamado `verificarCuenta()`.

Un método que haga test de dicho método podría ser `testVerificarCuentaMatchPasswordDado()`. Es un test que evalúa ese método para controlar si hay coincidencia de passwords.

### Herencia de los tests

Toda clase de test debe heredar de la clase `PHPUnit\Framework\TestCase`.

## Primer test

TDD exige por metodología que se escribe el test, y luego el código mas sencillo, directo y limpio que pase dicho test. No es lo mismo escribir código testeable o testear, que hacer TDD.

Este primer test solo testea el entorno y carece de utilidad practica. Pero esta bueno para ver que todo esta instalado y configurado correctamente.

Archivo `tests/StupidTest.php`:

```php
<?php

use PHPUnit\Framework\TestCase;

class StupidTest extends TestCase
{
    public function testTrueIsTrue()
    {
        $foo = true;
        $this->assertTrue($foo);
    }
}
```

El método solo testea si true es true. Uno de los poderes de la clase `TestCase` es la implementación de un conjunto de **asserts** o aserciones. Un listado exhaustivo puede encontrarse en la [documentación oficial](https://phpunit.readthedocs.io/en/9.1/assertions.html).

Ejecutar este test:

![First TDD Run](../attachments/tdd-notes/1-first-tdd-run.png)

La barra verde indica que el test que desarrollamos funciono correctamente. Si algo falla, veríamos una barra roja.

### Assertions

Las aserciones son sentencias que pueden ser evaluadas a *true/false*. En términos de lógica, es un predicado. Las *assertions* verifican si la sentencia evalúa a *true*. En dicho caso se cuenta como que el test esta Ok. Sino, se lanza un excepción y el test es marcado como que no pasa la prueba de validación.

Probemos hacer fallar un test. Agreguemos el siguiente método a nuestra clase `StupidTest`.

```php
public function testTrueIsNotFalse()
{
    $foo = false;
    $this->assertTrue($foo);
}
```

Al ejecutar dicho test, veremos una salida como la siguiente:

![Run PHPUnit with Failed Test](../attachments/tdd-notes/2-first-tdd-run-with-fail.png)

**Nota: quitar este ultimo método luego de probar este ejemplo, así no fallara en posteriores ejecuciones**

Vemos que PHPUnit ofrece una cantidad de ayuda contextual sobre el fallo. Archivo, numero de linea, nombre de clase+método que produjo el error.

## Empezando a testear

Creemos la clase URL en `src/core/Url.php`:

```php
<?php

namespace PhpUnitTutorial;

class Url
{
    public function sluggify($string, $separator = '-', $maxLength = 96)
    {
        $title = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $title = preg_replace("%[^-/+|\w ]%", '', $title);
        $title = strtolower(trim(substr($title, 0, $maxLength), '-'));
        $title = preg_replace("/[\/_|+ -]+/", $separator, $title);

        return $title;
    }
}
```

Y la correspondiente para testear `tests/core/UrlTest.php`:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    public function testSluggifyReturnsSluggifiedString()
    {
        $originalString = 'This string will be sluggified';
        $expectedResult = 'this-string-will-be-sluggified';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }
}
```

Ejecutando este test, obtenemos un warning debido a que no se implementa ningún test en la clase `UrlTest`.

Avancemos. Hagamos un test sobre el método `sluggify`. El slug de un string es un nuevo string todo en minúscula y con los espacios reemplazados por guiones medios.

Para que esto funcione, hacen falta algunos ligeros cambios en el archivo `phpunit.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<phpunit colors="true" bootstrap="vendor/autoload.php">
    <testsuites>
        <testsuite name="Application Test Suite">
            <directory>./tests/</directory>
        </testsuite>
    </testsuites>
</phpunit>
```

Y ademas generar el autoload de composer:

```bash
composer dump-autoload
```

Una vez realizadas dichas acciones, podemos ejecutar los tests:

![Run PHPUnit with autoload and real class](../attachments/tdd-notes/3-second-run.png)

### Agregando tests

Esa primer función de test comprueba el funcionamiento básico de la función, pero existen muchos casos a evaluar. ¿Que sucede si ingresan números? ¿Y caracteres no alfabéticos? Lo ideal es agregar para estos casos tantos tests como se pueda necesitar en el archivo `tests/core/UrlTest.php`:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Core\Url;

class UrlTest extends TestCase
{
    public function testSluggifyReturnsSluggifiedString()
    {
        $originalString = 'This string will be sluggified';
        $expectedResult = 'this-string-will-be-sluggified';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }

        public function testSluggifyReturnsExpectedForStringsContainingNumbers()
    {
        $originalString = 'This1 string2 will3 be 44 sluggified10';
        $expectedResult = 'this1-string2-will3-be-44-sluggified10';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }

    public function testSluggifyReturnsExpectedForStringsContainingSpecialCharacters()
    {
        $originalString = 'This! @string#$ %$will ()be "sluggified';
        $expectedResult = 'this-string-will-be-sluggified';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }

    public function testSluggifyReturnsExpectedForStringsContainingNonEnglishCharacters()
    {
        $originalString = "Tänk efter nu – förr'n vi föser dig bort";
        $expectedResult = 'tank-efter-nu-forrn-vi-foser-dig-bort';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }

    public function testSluggifyReturnsExpectedForEmptyStrings()
    {
        $originalString = '';
        $expectedResult = '';

        $url = new URL();

        $result = $url->sluggify($originalString);

        $this->assertEquals($expectedResult, $result);
    }
}
```

### Refactorizando los tests

Un posible problema a la hora de testear nuestras apps es que la batería de tests pueden crecer bastante. Y después de todo, también son código. Por lo tanto, puede estar sujeto a todos los problemas asociados a una evolución pobre de un modelo de objetos. El ejemplo visto hasta ahora de `UrlTest` no cumple con el principio **DRY**. Todos los métodos de test realizan funciones parecidas, pero sobre diferentes datos de entrada/salida.

Si pudiéramos hacer que el test se ejecute sobre una colección de pares de datos arbitrarios, que se sean provistos como parámetros de la función, podríamos refactorizar todos los test en uno solo.

PHPUnit ofrece este mecanismo a través de las *annotation* y de los métodos *data provider*.

Annotations o anotaciones son mecanismos que vía comentarios en el código proveen funcionalidad. PHPUnit realiza un uso exhaustivo de anotaciones, como puede encontrarse en [su documentación](https://phpunit.readthedocs.io/en/9.1/annotations.html).

En particular, nos interesa la anotación `@dataProvider`. Esta anotación recibe el nombre de un método de la clase Test, y debe retornar un array de arrays, donde cada uno de estos últimos debe tener tantos elementos como argumentos de la función (cada elemento sera pasado en ese orden a la función).

Refactoricemos ahora la clase `UrlTest` con estos conceptos:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Core\Url;

class UrlTest extends TestCase
{
    /**
     * @param string    $originalString String to be sluggified
     * @param string    $expectedResult What we expect our slug result to be
     * @dataProvider    providerTestSluggifyReturnsSluggifiedString
     */
    public function testSluggifyReturnsSluggifiedString($originalString, $expectedResult)
    {
        $url = new URL();
        $result = $url->sluggify($originalString);
        $this->assertEquals($expectedResult, $result);
    }

    public function providerTestSluggifyReturnsSluggifiedString()
    {
        return [
            [
                'This string will be sluggified',
                'this-string-will-be-sluggified'
            ],
            [
                'THIS STRING WILL BE SLUGGIFIED',
                'this-string-will-be-sluggified'
            ],
            [
                'This1 string2 will3 be 44 sluggified10',
                'this1-string2-will3-be-44-sluggified10'
            ],
            [
                'This! @string#$ %$will ()be "sluggified',
                'this-string-will-be-sluggified'
            ],
            [
                "Tänk efter nu – förr'n vi föser dig bort",
                'tank-efter-nu-forrn-vi-foser-dig-bort'
            ],
            [
                '',
                ''
            ],
        ];
    }
}
```

Esto permite, ademas, agregar nuevos casos a futuro que puedan ir apareciendo, con solo agregarlos al método provider correspondiente, ingresaran a la suite de tests sin mayor problema que agregar el caso al array de dicho método.

### Testear métodos privados

Un problema puede ser realizar test de métodos que son privados al ámbito de una clase. Al tener que llamarlos desde la clase de test, obtendríamos un error por ser un método, justamente, privado.

Vamos a poner por ejemplo una clase `PhpUnitTutorial\Models\User` y su correspondiente test.

Archivo `src/Models/User.php`:

```php
<?php

namespace PhpUnitTutorial\Models;

class User
{
    const MIN_PASS_LENGTH = 8;
    private $user = [];

    public function __construct(array $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setPassword($password)
    {
        if (strlen($password) < self::MIN_PASS_LENGTH) {
            return false;
        }

        $this->user['password'] = $this->cryptPassword($password);

        return true;
    }

    private function cryptPassword($password)
    {
        return md5($password);
    }
}
```

Archivo `tests/Model/UserTest.php`:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Models\User;

class UserTest extends TestCase
{
    //
}
```

Esto dará un warning como en el caso anteriormente visto.

Para testear los métodos privados a veces se pueden hacer de forma indirecta, por ejemplo, en la clase `User` el método `cryptPassword` no puede ser testeado, pero revisando los métodos `setPassword` y `getUser` podemos indirectamente ejecutarlo y hacer la comparación de su resultado.

Podemos entonces realizar estos 2 tests:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Models\User;

class UserTest extends TestCase
{
    public function testSetPasswordReturnsTrueWhenPasswordSuccessfullySet()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $result = $user->setPassword($password);
        $this->assertTrue($result);
    }

    public function testGetUserReturnsUserWithExpectedValues()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $user->setPassword($password);
        # Se puede calcular el md5 de un string por consola con
        #   echo -n 'foobarbaz' | md5sum
        $expectedPasswordResult = '6df23dc03f9b54cc38a0fc1483df6e21';
        $currentUser = $user->getUser();
        $this->assertEquals($expectedPasswordResult, $currentUser['password']);
    }
}
```

De forma indirecta, estamos testeando ademas el método `User::cryptPassword()`. ¿Pero que pasa en casos donde no se da una relación tan directa entre entrada y salida de 2 métodos?

Es posible en tiempo de ejecución hacer que un método privado pase a ser publico. Se puede generar un método genérico en la clase `UserTest` que haga exactamente esto:

Archivo `UserTest.php`:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Models\User;

class UserTest extends TestCase
{
    public function testSetPasswordReturnsTrueWhenPasswordSuccessfullySet()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $result = $user->setPassword($password);
        $this->assertTrue($result);
    }

    public function testGetUserReturnsUserWithExpectedValues()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $user->setPassword($password);
        $expectedPasswordResult = '6df23dc03f9b54cc38a0fc1483df6e21';
        $currentUser = $user->getUser();
        $this->assertEquals($expectedPasswordResult, $currentUser['password']);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testCryptPasswordReturnsTrueWhenStringSuccessfullyHashed()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $crypted_string = $this->invokeMethod($user, 'cryptPassword', [$password]);
        $expectedCryptResult = '6df23dc03f9b54cc38a0fc1483df6e21';
        $this->assertEquals($expectedCryptResult, $crypted_string);
    }
}
```

Usando reflexión, que permite analizar y modificar definiciones de objetos en tiempo de ejecución, puedo realizar la invocación del método privado haciéndolo accesible (ver `$method->setAccessible(true)`).

## Code Coverage

La cobertura de código permite generar un reporte que indique de forma sencilla que porcentaje de nuestro código se encuentra cubierto por algún test. Es muy útil generar estos reportes para saber que proporción de nuestra app esta siendo contemplada por los test que vamos construyendo.

El reporte de cobertura suele generarse en html, y es necesario introducir una nueva sección en el archivo `phpunit.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<phpunit colors="true" bootstrap="vendor/autoload.php">
    <testsuites>
        <testsuite name="Application Test Suite">
            <directory>./tests/</directory>
        </testsuite>
    </testsuites>
    <filter>
        <whitelist processUncoveredFilesFromWhitelist="true">
            <directory suffix=".php">./src</directory>
        </whitelist>
    </filter>
    <logging>
        <log type="coverage-html" target="/tmp/coverage-report" lowUpperBound="35" highLowerBound="70"/>
    </logging>
</phpunit>
```

Ejecutando entonces `phpunit`, se obtiene un directorio en `/tmp/coverage-report` donde se encuentra un montón de archivos, dentro de los cuales, se puede ver el reporte desde el `index.html`. Ejemplo:

![Code Coverage Report Example](../attachments/tdd-notes/4-coverage-report-html-example.png)

Como el porcentaje depende del directorio que se este configurando, es posible configurar que directorios deben ser tenidos en cuenta y cuales deben excluirse (todo desde el archivo `phpunit.xml`).

¿Porque marca el reporte que hay un 75% de cobertura en funciones y métodos y cero en clases?, Ingresando en `Models\User.php` se puede observar el reporte:

![Code Coverage Incomplete Coverage Example](../attachments/tdd-notes/5-coverage-report-incomplete-example.png)

Lo que nos dice este reporte es que no existe ningún test que pruebe la linea 23 (En color rojo).

Es cuestión de agregar un test que justamente pruebe el caso ese y se retorne efectivamente falso.

El siguiente método debe ser agregado a `UserTest.php`:

```php
    public function testSetPasswordReturnsFalseWhenPasswordLengthIsTooShort()
    {
        $details = [];
        $user = new User($details);
        $password = 'fub';
        $result = $user->setPassword($password);
        $this->assertFalse($result);
    }
```

Ejecutando nuevamente el test, se obtiene una cobertura del 100%.

Existen diversas posturas sobre si se debe o no alcanzar un 100% de code coverage. Siempre es bueno tener una buena cantidad de test, sin que esto implique mas esfuerzo que el código en si, o testear infinidad de funciones o métodos sencillos, cuyos test son triviales y no aportan al resultado final. Algunos utilizan el indice CRAP (presente en el reporte de cobertura) para determinar que se realizan tests a partir de un umbral (por ejemplo, si el método tiene un indice CRAP menor a 5, no es necesario construir un test especifico). En cualquier caso, son convenciones.

## Testing de objetos mas complejos

A continuación se testeara una API de pagos.

Para ello primero se instalara una librería (obsoleta, para mantener compatibilidad con el tutorial):

```bash
composer require ajbdev/authorizenet-php-api
```

El tutorial propone a continuación trabajar contra una API de pagos. Un código típico para este tipo de procesamiento es:

```php
<?php

namespace PhpUnitTutorial\Models;

class Payment
{
    const API_ID = 123456;
    const TRANS_KEY = 'TRANSACTION KEY';

    public function processPayment(array $paymentDetails)
    {
        $transaction = new \AuthorizeNetAIM(self::API_ID, self::TRANS_KEY);
        $transaction->amount = $paymentDetails['amount'];
        $transaction->card_num = $paymentDetails['card_num'];
        $transaction->exp_date = $paymentDetails['exp_date'];

        $response = $transaction->authorizeAndCapture();

        if ($response->approved) {
            return $this->savePayment($response->transaction_id);
        } else {
            throw new \Exception($response->error_message);
        }
    }

    public function savePayment($transactionId)
    {
        // Logic for saving transaction ID to database or anywhere else would go in here
        return true;
    }
}
```

A continuación creamos el test asociado:

```php
<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Models\Payment;

class PaymentTest extends TestCase
{
    public function testProcessPaymentReturnsTrueOnSuccessfulPayment()
    {
        $paymentDetails = [
            'amount'   => 123.99,
            'card_num' => '4111-1111-1111-1111',
            'exp_date' => '03/2013',
        ];
        $payment = new Payment();
        $result = $payment->processPayment($paymentDetails);
        $this->assertTrue($result);
    }
}
```

Al intentar ejecutar este test, obtenemos el siguiente error:

![Error al ejecutar Test contra API](../attachments/tdd-notes/6-error-on-test-api-behavior.png)

En este caso, la API responde con error de credenciales. Si bien la opción podría ser ir al servicio y obtener credenciales validas, en realidad no es deseable que nuestros tests dependan de servicios externos. (Por ejemplo, si estuvieran caídos los servers de la API, nuestros tests fallarían, y no por culpa del código nuestro, que es en realidad lo que nos interesa testear).

Para lidiar con estos escenarios es que existen los mocks.

### Usando Objetos Mocks

Los Mocks son objetos "fake" que nos permiten manejar dependencias externas, reemplazandolas por objetos propios durante la etapa de ejecución de tests, y que responden lo que necesitemos sin realizar llamadas al servicio externo y por lo tanto, eliminando fuentes de errores ajenas a nuestro control.

Pero si observamos la linea donde se crea la instancia al cliente de la API, se ve que no es muy viable con el código actual reemplazar el objeto con uno propio:

```php
$transaction = new \AuthorizeNetAIM(self::API_ID, self::TRANS_KEY);
```

El problema es que nuestro código esta acoplado a la creación del objeto de la API, cuando no debería ser así.

La forma de lograr superar esto es mediante la inyección de dependencias. Una forma fácil de entender la inyección de dependencias, es que movemos la responsabilidad de crear un objeto fuera del método que deseamos testear, y se pasa la instancia del objeto al método en cuestión.

En nuestro caso, es tan sencillo como quitar el `new` del método `processPayment`. El método quedaría como sigue:

```php
public function processPayment(\AuthorizeNetAIM $transaction, array $paymentDetails)
{
    $transaction->amount = $paymentDetails['amount'];
    $transaction->card_num = $paymentDetails['card_num'];
    $transaction->exp_date = $paymentDetails['exp_date'];

    $response = $transaction->authorizeAndCapture();

    if ($response->approved) {
        return $this->savePayment($response->transaction_id);
    } else {
        throw new \Exception($response->error_message);
    }
}
```

Existe 1 ventaja y 1 problema en la definición del metodo anterior. Para poder hacer andar el ejemplo, alcanza con crear un nuevo objeto que extienda de la clase `AuthorizeNetAIM`, y sobreescribir los metodos problematicos, haciendo que retornen los resultados deseados. ¿Cual es el inconveniente con esto? En proyectos chicos, ninguno, y es de hecho una buena idea. ¿Pero que hacer en proyectos grandes, con, por ejemplo, mas de 20 servicios externos con sus clases y dependencias? Se torna complejo extender objetos fake solo para sobreescribir metodos puntuales de cara a los tests. Necesitamos alguna herramienta que nos permita acelerar esta creación de Mocks. Y aca entra nuevamente PHPUnit.

Lo que nos falta modificar es nuestro test, donde ahora debemos crear un objeto transacción, pero que no queremos que sea identico al que se creaba antes en el metodo `processPayment`. El siguiente codigo no es lo que buscamos:

```php
public function testProcessPaymentReturnsTrueOnSuccessfulPayment()
{
    $paymentDetails = array(
        'amount'   => 123.99,
        'card_num' => '4111-1111-1111-1111',
        'exp_date' => '03/2013',
    );

    $payment = new Payment();

    $authorizeNet = new \AuthorizeNetAIM($payment::API_ID, $payment::TRANS_KEY);

    $result = $payment->processPayment($authorizeNet, $paymentDetails);

    $this->assertTrue($result);
}
```

Ahora bien, PHPUnit nos provee un helper para crear mocks:

```php
$authorizeNet = $this->getMockBuilder('\AuthorizeNetAIM')
    ->setConstructorArgs(array($payment::API_ID, $payment::TRANS_KEY))
    ->getMock();
```

Lo interesante, es que este builder de mocks crea un nuevo objeto, muy similar al original, pero con un detalle, cualquier método que se invoque, retornara null. Pues todos su código fue eliminado (salvo la declaración y parámetros). A estos métodos se los conoce como métodos `stub`. Ahora bien, no siempre es deseable que retornen `null` estos métodos. Es posible asignar un valor de retorno.

```php
$authorizeNet = $this->getMockBuilder('\AuthorizeNetAIM')
    ->setConstructorArgs(array($payment::API_ID, $payment::TRANS_KEY))
    ->getMock();

$authorizeNet->expects($this->once())
    ->method('authorizeAndCapture')
    ->will($this->returnValue('RETURN VALUE HERE!'));
```

`expects`: Indica la cantidad de veces que se espera que el método sea invocado.

`method`: El método a sobre-escribir el valor de retorno.

`will` y `returnValue`: El valor de retorno.
