<?php

namespace PhpUnitTutorial\Test;

use PHPUnit\Framework\TestCase;
use PhpUnitTutorial\Models\User;

class UserTest extends TestCase
{
    public function testSetPasswordReturnsTrueWhenPasswordSuccessfullySet()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $result = $user->setPassword($password);
        $this->assertTrue($result);
    }

    public function testGetUserReturnsUserWithExpectedValues()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $user->setPassword($password);
        $expectedPasswordResult = '6df23dc03f9b54cc38a0fc1483df6e21';
        $currentUser = $user->getUser();
        $this->assertEquals($expectedPasswordResult, $currentUser['password']);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testCryptPasswordReturnsTrueWhenStringSuccessfullyHashed()
    {
        $details = [];
        $user = new User($details);
        $password = 'foobarbaz';
        $crypted_string = $this->invokeMethod($user, 'cryptPassword', [$password]);
        $expectedCryptResult = '6df23dc03f9b54cc38a0fc1483df6e21';
        $this->assertEquals($expectedCryptResult, $crypted_string);
    }

    public function testSetPasswordReturnsFalseWhenPasswordLengthIsTooShort()
    {
        $details = [];
        $user = new User($details);
        $password = 'fub';
        $result = $user->setPassword($password);
        $this->assertFalse($result);
    }
}
